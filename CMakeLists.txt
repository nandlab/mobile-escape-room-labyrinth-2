cmake_minimum_required(VERSION 3.5)

set(PROJNAME "mobile-escape-room-labyrinth-2")

project(${PROJNAME} LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Boost REQUIRED program_options)
find_package(SFML REQUIRED audio)

add_subdirectory(pigpiopp)

add_executable(${PROJNAME} main.cpp mobile-escape-room-labyrinth.cpp mobile-escape-room-labyrinth.hpp)
target_link_libraries(${PROJNAME} PRIVATE pigpiopp Boost::program_options sfml-audio)
