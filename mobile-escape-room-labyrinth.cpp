#include "mobile-escape-room-labyrinth.hpp"
#include <cstdint>
#include <iostream>

namespace MobileEscapeRoom
{
typedef Labyrinth self;

self::Labyrinth(std::vector<unsigned> input_pins,
          unsigned reset_pin,
          unsigned exit_pin,
          std::vector<unsigned> output_pins,
          std::vector<std::string> sounds)
    : mutex(),
      is_ready(false),
      cv_ready(),
      input_pins(std::move(input_pins)),
      reset_pin(reset_pin),
      exit_pin(exit_pin),
      output_pins(std::move(output_pins)),
      sound_buffers(this->input_pins.size()),
      sounds(this->input_pins.size()),
      riddles_solved(this->input_pins.size()),
      pigpio()
{
    if (this->input_pins.size() != this->output_pins.size()) {
        throw std::runtime_error("Number of input and output pins does not match!");
    }
    if (this->input_pins.size() != sounds.size()) {
        throw std::runtime_error("Number of pins and sounds does not match!");
    }
    for (std::size_t i = 0; i < sounds.size(); i++) {
        bool success = sound_buffers[i].loadFromFile(sounds[i]);
        if (!success) {
            throw std::runtime_error("Cannot load sound " + sounds[i]);
        }
        this->sounds[i].setBuffer(sound_buffers[i]);
    }
}

bool self::all_riddles_solved() {
    for (bool solved : riddles_solved) {
        if (!solved) {
            return false;
        }
    }
    return true;
}

template<typename CharT, typename Traits, typename ElemT>
static std::basic_ostream<CharT, Traits>& operator <<(std::basic_ostream<CharT, Traits> &ostream, const std::vector<ElemT> &vector) {
    ostream << '[';
    bool first = true;
    for (const ElemT &e : vector) {
        if (!first) {
            ostream << ", ";
        }
        ostream << e;
        first = false;
    }
    return ostream << ']';
}

#define STEADY_DUR 25000

void self::run() {
    using std::cout;
    using std::endl;
    using std::uint32_t;
    std::unique_lock<std::mutex> lock(mutex);
    cout << "Starting mobile escape room program..." << endl;
    cout << "Input pins: " << input_pins << endl;
    cout << "Reset pin: " << reset_pin << endl;
    cout << "Exit pin: " << exit_pin << endl;
    cout << "Output pins: " << output_pins << endl;
    pigpio.start(nullptr, nullptr);

    for (std::size_t i = 0; i < input_pins.size(); i++) {
        const unsigned input_pin = input_pins[i];
        const unsigned output_pin = output_pins[i];
        pigpio.set_mode(input_pin, pigpiopp::INPUT);
        pigpio.set_mode(output_pin, pigpiopp::OUTPUT);

        pigpio.set_glitch_filter(input_pin, STEADY_DUR);
        pigpio.register_callback(input_pin, pigpiopp::FALLING_EDGE, [this, i](int a1, int a2, uint32_t a3) {
            (void) a1;
            (void) a2;
            (void) a3;
            std::lock_guard<std::mutex> lock(mutex);
            if (!riddles_solved[i]) {
                cout << "Riddle " << i << " solved" << endl;
                riddles_solved[i] = true;
                sounds[i].play();
                if (all_riddles_solved()) {
                    cout << "All riddles are solved, waiting for reset or exit..." << endl;
                }
                pigpio.write(output_pins[i], 1);
            }
        });
    }

    pigpio.set_glitch_filter(reset_pin, STEADY_DUR);
    pigpio.register_callback(reset_pin, pigpiopp::FALLING_EDGE, [this](int a1, int a2, uint32_t a3) {
        (void) a1;
        (void) a2;
        (void) a3;
        std::lock_guard<std::mutex> lock(mutex);
        cout << "Resetting all riddles" << endl;
        for (size_t i = 0; i < riddles_solved.size(); i++) {
            riddles_solved[i] = false;
        }
    });

    pigpio.set_glitch_filter(exit_pin, STEADY_DUR);
    pigpio.register_callback(exit_pin, pigpiopp::FALLING_EDGE, [this](int a1, int a2, uint32_t a3) {
        (void) a1;
        (void) a2;
        (void) a3;
        std::lock_guard<std::mutex> lock(mutex);
        cout << "Exiting" << endl;
        is_ready = true;
        cv_ready.notify_all();
    });

    cv_ready.wait(lock, [this]{return is_ready;});
}
}
