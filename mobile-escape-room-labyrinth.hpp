#ifndef MOBILEESCAPEROOMLABYRINTH_HPP
#define MOBILEESCAPEROOMLABYRINTH_HPP

#include <pigpiopp.h>
#include <vector>
#include <stdexcept>
#include <mutex>
#include <condition_variable>
#include <string>
#include <SFML/Audio.hpp>
#include <cstdint>
#include <iostream>

namespace MobileEscapeRoom
{

class Labyrinth
{
    std::mutex mutex;
    bool is_ready;
    std::condition_variable cv_ready;
    std::vector<unsigned> input_pins;
    unsigned reset_pin;
    unsigned exit_pin;
    std::vector<unsigned> output_pins;
    std::vector<sf::SoundBuffer> sound_buffers;
    std::vector<sf::Sound> sounds;
    std::vector<bool> riddles_solved;
    pigpiopp::client pigpio;

public:
    Labyrinth(std::vector<unsigned> input_pins,
              unsigned reset_pin,
              unsigned exit_pin,
              std::vector<unsigned> output_pins,
              std::vector<std::string> sounds);

    void run();

private:
    bool all_riddles_solved();
};

}

#endif // MOBILEESCAPEROOMLABYRINTH_HPP
